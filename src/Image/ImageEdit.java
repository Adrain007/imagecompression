package Image;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImageEdit {
    public double[][] getRGB(HashMap<Integer, Color[][]> map) {
        Color color;
        double[][] mas = new double[map.size()][map.get(0).length * map.get(0)[0].length * 3];
        int len = map.get(0).length;
        for (int count : map.keySet()) {
            for (int idy = 0; idy < map.get(count).length; idy++) {
                for (int idx = 0; idx < map.get(count)[0].length; idx++) {
                    color = map.get(count)[idy][idx];
                    mas[count][idy * len * 3 + idx * 3] = color.getRed();
                    mas[count][idy * len * 3 + idx * 3 + 1] = color.getGreen();
                    mas[count][idy * len * 3 + idx * 3 + 2] = color.getBlue();
                }
            }
        }
        return mas;
    }

    private Color[][] meanValue(Color[][] square, int dimension) {
        double red = 0, green = 0, blue = 0;
        Color[][] mas = new Color[dimension][dimension];
        for (int idy = 0; idy < square.length - 1; idy++) {
            for (int idx = 0; idx < square[0].length - 1; idx++) {
                for (int y = idy; y < idy + 2; y++) {
                    for (int x = idx; x < idx + 2; x++) {
                        Color color = square[y][x];
                        red += color.getRed();
                        green += color.getGreen();
                        blue += color.getBlue();
                    }
                }
                mas[idy][idx] = new Color(red / 4, green / 4, blue / 4, 1);
                red = 0;
                green = 0;
                blue = 0;
            }
        }
        return mas;
    }

    public HashMap<Integer, Color[][]> getMean(HashMap<Integer, Color[][]> square) {
        HashMap<Integer, Color[][]> mean = new HashMap<>();
        for (int idx : square.keySet()) {
            Color [][] buf1 = meanValue(square.get(idx),3);
            //Color [][] buf2 = meanValue(buf1,6);
            mean.put(idx, meanValue(buf1, 2));
        }
        return mean;
    }

    /*public HashMap<Integer, Color[][]> getSquare(String url,int dimension) {
        int count = 0;
        HashMap<Integer, Color[][]> map = new HashMap<>();
        Image image = new Image("train_photo/" + url);
        PixelReader pixelReader = image.getPixelReader();
        for (int idy = 0; idy <= image.getHeight()-dimension; idy++){
            for (int idx = 0; idx <= image.getWidth()-dimension; idx++) {
                Color[][] square = new Color[dimension][dimension];
                for (int y = idy; y < idy + dimension; y++) {
                    for (int x = idx; x < idx + dimension; x++) {
                        square[y - idy][x - idx] = pixelReader.getColor(x, y);
                    }
                }
                map.put(count, square);
                count++;
            }
        }
        return map;
    }*/

    public HashMap<Integer, Color[][]> getSquare(String url, int dimension) throws FileNotFoundException {
        int count = 0;
        HashMap<Integer, Color[][]> map = new HashMap<>();
        Image image = new Image(new FileInputStream(url));
        PixelReader pixelReader = image.getPixelReader();
        for (int idy = 0; idy < image.getHeight(); idy += dimension) {
            for (int idx = 0; idx < image.getWidth(); idx += dimension) {
                Color[][] square = new Color[dimension][dimension];
                for (int y = idy; y < idy + dimension; y++) {
                    for (int x = idx; x < idx + dimension; x++) {
                        square[y - idy][x - idx] = pixelReader.getColor(x, y);
                    }
                }
                map.put(count, square);
                count++;
            }
        }
        return map;
    }

    public File getImage(double[][] mass, int width, int height, String format, int dimension, String fileName) {
        File file = new File(fileName);
        WritableImage image = new WritableImage(width, height);

        List<Color> colorList = new ArrayList<>();
        PixelWriter writer = image.getPixelWriter();
        int max = (int) Math.pow(dimension, 2);
        for (double[] mas : mass) {
            for (int j = 0; j < max * 3; j = j + 3) {
                Color color = new Color(mas[j], mas[j + 1], mas[j + 2], 1);
                colorList.add(color);
            }
        }
        int count = 0;
        for (int idy = 0; idy <= height - dimension; idy += dimension) {
            for (int idx = 0; idx <= width - dimension; idx += dimension) {
                for (int y = idy; y < idy + dimension; y++) {
                    for (int x = idx; x < idx + dimension; x++) {
                        writer.setColor(x, y, colorList.get(count));
                        count++;
                    }
                }
            }
        }
        if (format.equals("jpeg")) {
            try {
                ImageWriter imageWriter = ImageIO.getImageWritersByFormatName("JPEG").next();
                ImageWriteParam iwp = imageWriter.getDefaultWriteParam();
                iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                iwp.setCompressionQuality(1);
                BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image,null);
                FileImageOutputStream fio = new FileImageOutputStream(file);
                imageWriter.setOutput(fio);
                IIOImage iioImage = new IIOImage(bufferedImage, null, null);
                imageWriter.write(null, iioImage, iwp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
                BufferedImage imageRGB = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.OPAQUE);
                Graphics2D graphics = imageRGB.createGraphics();
                graphics.drawImage(bufferedImage, 0, 0, null);
                ImageIO.write(imageRGB, format, file);
                /*ImageIO.write(SwingFXUtils.fromFXImage(image,null),"png",file);*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
