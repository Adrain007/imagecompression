package NeuralNet;

import Image.ImageEdit;
import JavaFX.Main;
import javafx.scene.paint.Color;

import java.io.*;
import java.util.HashMap;
import java.util.Objects;

public class Study1 {
    private ImageEdit edit = new ImageEdit();
    private NeuralNet net1;
    private double step = 0.1;
    private File dir = new File("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\src\\train_photo");
    private File dir1 = new File("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\src\\Compressed");

    public Study1(NeuralNet net1){
        this.net1 = net1;
    }
    public void initNet() throws FileNotFoundException {
        this.net1.setAnswers(edit.getRGB(edit.getSquare(dir.getAbsolutePath()+"\\7083.jpg",4)));
        this.net1.setPatterns(edit.getRGB(edit.getMean(edit.getSquare(dir.getAbsolutePath()+"\\7083.jpg",4))));
        double[][] patterns = net1.getPatterns();
        net1.initHidden(patterns[0]);
        net1.countHiddenLayerOut();
        net1.initOutputLayer();
    }

    public void study() throws FileNotFoundException {
        double[][] patterns;
        double gErr;
        int numOfImage = 0;
        if (dir.isDirectory()) {
            do {
                for (String file : Objects.requireNonNull(dir.list())) {
                    HashMap<Integer,Color[][]> map = edit.getSquare(dir.getAbsolutePath()+"\\"+file,4);
                    HashMap<Integer,Color[][]> map1 = edit.getSquare(dir1.getAbsolutePath()+"\\"+file,3);
                    patterns = edit.getRGB(map1);
                    net1.setAnswers(edit.getRGB(map));
                    gErr=0;
                    for (int numOfPatterns = 0; numOfPatterns < patterns.length; numOfPatterns++) {
                        net1.setHiddenLayerInputs(patterns[numOfPatterns]);
                        net1.countHiddenLayerOut();
                        net1.setOutputLayerInputs();
                        net1.countOutput();
                        gErr+=net1.ERROR(net1.getAnswers()[numOfPatterns]);
                        //System.out.println(gErr+" - step: "+step);
                        for (int idx = 0; idx < net1.getOutputLayer().size(); idx++) {
                            Neuron neuron = net1.getOutputLayer().get(idx);
                            net1.getOutputLayerErr()[idx] = neuron.errorOutNeuron(neuron.Output(), net1.getAnswers()[numOfPatterns][idx]);
                            //System.out.println("errO - "+net2.getOutputLayerErr()[idx]);
                        }
                        for (int idx = 0; idx < net1.getHiddenLayer().size(); idx++) {
                            Neuron neuron = net1.getHiddenLayer().get(idx);
                            net1.getHiddenLayerErr()[idx] = neuron.errorHiddenNeuron(net1.getOutWeight(idx), net1.getOutputLayerErr());
                        }

                        for (int idx = 0; idx < net1.getOutputLayer().size(); idx++) {
                            Neuron neuron = net1.getOutputLayer().get(idx);
                            double[] outputWeight = neuron.getWeights();
                            double[] newWeight = new double[outputWeight.length];
                            for (int j = 0; j < outputWeight.length; j++) {
                                Neuron hiddenNeuron = net1.getHiddenLayer().get(j);
                                newWeight[j] = outputWeight[j] + step * net1.getOutputLayerErr()[idx] * hiddenNeuron.Output();
                            }
                            neuron.setWeights(newWeight);
                        }
                        for (int idx = 0; idx < net1.getHiddenLayer().size(); idx++) {
                            Neuron neuron = net1.getHiddenLayer().get(idx);
                            double[] hiddenWeight = neuron.getWeights();
                            double[] newWeight = new double[hiddenWeight.length];
                            double[] inputNeuron = patterns[numOfPatterns];
                            for (int j = 0; j < hiddenWeight.length; j++) {
                                newWeight[j] = hiddenWeight[j] + step * net1.getHiddenLayerErr()[idx] * inputNeuron[j];
                            }
                            neuron.setWeights(newWeight);
                        }

                        //System.out.println(net2.ERROR(net2.getAnswers()[numOfPatterns]));
                        //System.out.println(numOfPatterns);
                    }
                    numOfImage++;
                    System.out.println("изображение - " + numOfImage);
                    System.out.println("Error - " + gErr);
                    if(gErr<1000) {
                        try {
                            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("Net2.ser"));
                            os.writeObject(net1);
                            System.out.println("Сохранение!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            new Main().test(net1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
                numOfImage=0;
                //System.out.println("Error : "+gErr);
            } while (true);
        }
    }
}
