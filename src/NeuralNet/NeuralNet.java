package NeuralNet;

import java.io.Serializable;
import java.util.ArrayList;

public class NeuralNet implements Serializable {
    private static final long serialVersionUID = 1372545293329104584L;
    private double[] inputLayer;
    private double[] hiddenLayerOut;
    private double[] outputLayerOut;
    private double[] hiddenLayerErr;
    private double[] outputLayerErr;
    private ArrayList<Neuron> hiddenLayer;
    private ArrayList<Neuron> outputLayer;
    private double[][] patterns;
    private double[][] answers;

    public NeuralNet(int input, int hidden, int out) {
        inputLayer = new double[input];
        hiddenLayerOut = new double[hidden];
        hiddenLayerErr = new double[hidden];
        outputLayerOut = new double[out];
        outputLayerErr = new double[out];
    }

    double[][] getPatterns() {
        return patterns;
    }

    double[] getHiddenLayerOut() {
        return hiddenLayerOut;
    }

    double[] getHiddenLayerErr() {
        return hiddenLayerErr;
    }

    double[] getOutputLayerErr() {
        return outputLayerErr;
    }

    ArrayList<Neuron> getHiddenLayer() {
        return hiddenLayer;
    }

    ArrayList<Neuron> getOutputLayer() {
        return outputLayer;
    }

    double[] getOutputLayerOut() {
        return outputLayerOut;
    }

    double[][] getAnswers() {
        return answers;
    }


    private double[] initWeight(int numOfEl) {
        double[] weight = new double[numOfEl];
        for (int i = 0; i < weight.length; i++) {
            weight[i] = Math.random() * (-2) + 1;
        }
        return weight;
    }

    void initHidden(double[] input) {
        hiddenLayer = new ArrayList<>();
        for (double aHiddenLayerOut : hiddenLayerOut) {
            hiddenLayer.add(new Neuron(input, initWeight(inputLayer.length)));
        }
    }

    final void initOutputLayer() {
        outputLayer = new ArrayList<>();
        for (double anOutputLayerOut : outputLayerOut) {
            outputLayer.add(new Neuron(hiddenLayerOut, initWeight(hiddenLayerOut.length)));
        }
    }

    final void countHiddenLayerOut() {
        int index = 0;
        for (Neuron neuron : hiddenLayer) {
            hiddenLayerOut[index] = neuron.Output();
            index++;
        }
    }

    void countOutput() {
        int index = 0;
        for (Neuron neuron : outputLayer) {
            outputLayerOut[index] = neuron.Output();
            index++;
        }
    }

    double ERROR(double[] answer) {
        double sum = 0;
        for (int i = 0; i < answer.length; i++) {
            sum += Math.pow(answer[i] - outputLayerOut[i], 2);
        }
        return 0.5 * sum;
    }

    double[] getOutWeight(int index) {
        double[] weight = new double[answers[0].length];
        int j = 0;
        for (Neuron neuron : outputLayer) {
            weight[j] = neuron.getWeight(index);
            j++;
        }
        return weight;
    }
    double[] getHiddenWeight(int index) {
        double[] weight = new double[hiddenLayer.size()];
        int j = 0;
        for (Neuron neuron : hiddenLayer) {
            weight[j] = neuron.getWeight(index);
            j++;
        }
        return weight;
    }

    void setHiddenLayerInputs(double[] hiddenLayerInputs) {
        for (Neuron hiddenNeuron : hiddenLayer) {
            hiddenNeuron.setInputs(hiddenLayerInputs);
        }
    }

    void setOutputLayerInputs() {
        for (Neuron outputNeuron : outputLayer) {
            outputNeuron.setInputs(hiddenLayerOut);
        }
    }

    void setPatterns(double[][] patterns) {
        this.patterns = patterns;
    }

    void setAnswers(double[][] answers) {
        this.answers = answers;
    }

    public void setInputLayer(double[] inputLayer){
        this.inputLayer = inputLayer;
    }

    public double[] directPass(){
        for (Neuron hiddenNeuron: hiddenLayer){
            hiddenNeuron.setInputs(inputLayer);
        }
        countHiddenLayerOut();
        for (Neuron outputNeuron: outputLayer){
            outputNeuron.setInputs(hiddenLayerOut);
        }
        countOutput();
        return getOutputLayerOut();
    }
}
