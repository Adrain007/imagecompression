package JavaFX;

import Image.ImageEdit;
import NeuralNet.NeuralNet;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.*;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        /*Study study = new Study(new NeuralNet(48,40,12),new NeuralNet(12,40,48));
        study.initNet();
        study.study();*/
        /*ObjectInputStream is1 = new ObjectInputStream(new FileInputStream("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\Net1.ser"));
        NeuralNet net1 = (NeuralNet) is1.readObject();
        ObjectInputStream is2 = new ObjectInputStream(new FileInputStream("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\Net2.ser"));
        NeuralNet net2 = (NeuralNet) is2.readObject();
        Study study = new Study(net1,net2);
        study.study();*/
        Parent root = FXMLLoader.load(getClass().getResource("GUI.fxml"));
        primaryStage.setTitle("Image Compression");
        primaryStage.getIcons().add(new Image("icons\\icon.png"));
        Scene scene = new Scene(root, 600,335);
        scene.getStylesheets().add(getClass().getResource("CSS\\interface.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public void test(NeuralNet net1) throws IOException {
        ImageEdit edit = new ImageEdit();
        double[][] patterns = edit.getRGB(edit.getSquare("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\src\\Compressed\\COCO_train2014_000000000064.jpg",3));
        double[][] masOut = new double[patterns.length][48];
        int x = 0;
        int y = 0;
        for(double[] pat: patterns){
            net1.setInputLayer(pat);
            double[] buf = net1.directPass();
            for(double d: buf) {
                masOut[x][y] = d;
                y++;
            }
            x++;
            y=0;
        }
        edit.getImage(masOut, 480, 320,"jpg",4,"sdf");
    }
    public void test(NeuralNet net1, NeuralNet net2) throws IOException {
        ImageEdit edit = new ImageEdit();
        double[][] patterns = edit.getRGB(edit.getSquare("C:\\Users\\Adrian\\IdeaProjects\\ImageCompression\\src\\train_photo\\COCO_train2014_000000000064.png",4));
        double[][] mas = new double[patterns.length][patterns[0].length];
        double[][] masOut = new double[patterns.length][48];
        int x = 0;
        int y = 0;
        //System.out.println();
        for (double[] pattern : patterns) {
            net1.setInputLayer(pattern);
            masOut[x] = net1.directPass();
            net2.setInputLayer(masOut[x]);
            for (double d : net2.directPass()) {
                mas[x][y] = d;
                //System.out.print(d * 255 + " ");
                y++;
            }
            y = 0;
            //System.out.println();
            x++;
        }
        edit.getImage(mas, 480, 640,"png",4,"test212.png");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
