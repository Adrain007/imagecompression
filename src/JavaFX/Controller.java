package JavaFX;

import Image.ImageEdit;
import NeuralNet.NeuralNet;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;

import java.io.ObjectInputStream;
import java.util.List;


public class Controller {

    private Thread thread;

    private String savePath = System.getProperty("user.home")+"\\Desktop";

    private ContextMenu prevContextMenu;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private MenuItem saveMenuItem, quitMenuItem, openMenuItem;

    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private ListView<File> listIn, listOut;

    @FXML
    private ComboBox<String> comboBox;


    @FXML
    private void initialize() {
        initChoiceBox();
    }

    @FXML
    private void onDragDropped(DragEvent event) {
        List<File> list = event.getDragboard().getFiles();
        for (File file : list) {
            if (!listIn.getItems().contains(file)) {
                listIn.getItems().add(file);
            }
        }
    }

    @FXML
    private void onDragOver(DragEvent event) {
        if (event.getDragboard().hasFiles()) {
            event.acceptTransferModes(TransferMode.ANY);
        }
    }

    private void initChoiceBox() {
        comboBox.getItems().addAll("Compress", "Decompress");
        comboBox.setValue("Compress");
    }

    @FXML
    private void deleteSelectedItems() {
        ObservableList<File> list;
        if (listIn.isFocused()) {
            list = listIn.getSelectionModel().getSelectedItems();
            listIn.getItems().removeAll(list);
        } else if (listOut.isFocused()) {
            list = listOut.getSelectionModel().getSelectedItems();
            listOut.getItems().removeAll(list);
        }
    }

    @FXML
    private void selectAllItems() {
        if (listIn.isFocused()) {
            MultipleSelectionModel<File> selectionModel = listIn.getSelectionModel();
            selectionModel.setSelectionMode(SelectionMode.MULTIPLE);
            listIn.getSelectionModel().selectAll();
        } else if (listOut.isFocused()) {
            MultipleSelectionModel<File> selectionModel1 = listOut.getSelectionModel();
            selectionModel1.setSelectionMode(SelectionMode.MULTIPLE);
            listOut.getSelectionModel().selectAll();
        }
    }

    @FXML
    private void copySelectedItems() {
        ObservableList<File> list = FXCollections.observableArrayList();
        if (listIn.isFocused()) {
            list = listIn.getSelectionModel().getSelectedItems();
        } else if (listOut.isFocused()) {
            list = listOut.getSelectionModel().getSelectedItems();
        }
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putFiles(list);
        clipboard.setContent(content);
    }

    @FXML
    private void pasteSelectedItems() {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        List<File> list = FXCollections.observableArrayList();
        if (clipboard.hasFiles()) {
            list = clipboard.getFiles();
        }
        for(File file: list) {
            if (!listIn.getItems().contains(file)) {
                listIn.getItems().add(file);
            }
        }
    }

    @FXML
    private void openMenu() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select open file");
        List<File> file = chooser.showOpenMultipleDialog(openMenuItem.getParentPopup().getOwnerWindow());
        if (file != null) {
            listIn.getItems().addAll(file);
        }
    }

    @FXML
    private void saveMenu() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select directory for save");
        chooser.setInitialDirectory(new File(System.getProperty("user.home")+"\\Desktop"));
        File newFile = chooser.showDialog(saveMenuItem.getParentPopup().getOwnerWindow());
        if (newFile!=null) {
            savePath = newFile.getPath();
        }
        System.out.println(savePath);
    }

    @FXML
    private void quitMenu() {
        Stage stage = (Stage) quitMenuItem.getParentPopup().getOwnerWindow();
        stage.close();
    }

    @FXML
    private void rightMouseClick(MouseEvent event) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copy = new MenuItem("Copy");
        MenuItem paste = new MenuItem("Paste");
        MenuItem delete = new MenuItem("Delete");
        MenuItem selectAll = new MenuItem("Select All");
        MenuItem add = new MenuItem("Add");
        if (event.getSource() instanceof ListView) {
            String id = ((ListView) event.getSource()).getId();
            if (id.equals(listIn.getId())) {
                contextMenu.getItems().addAll(add,copy, paste, delete, selectAll);
            } else {
                contextMenu.getItems().addAll(copy, delete, selectAll);
            }
        }
        add.setOnAction(event1 -> openMenu());
        copy.setOnAction(event1 -> copySelectedItems());
        paste.setOnAction(event1 -> pasteSelectedItems());
        delete.setOnAction(event1 -> deleteSelectedItems());
        selectAll.setOnAction(event1 -> selectAllItems());
        if (event.isSecondaryButtonDown()) {
            if (prevContextMenu != null) {
                prevContextMenu.hide();
            }
            contextMenu.show(anchorPane, event.getScreenX(), event.getScreenY());
            prevContextMenu = contextMenu;
        } else {
            if (prevContextMenu != null) {
                prevContextMenu.hide();
            }
        }
    }

    @FXML
    private void start() {
        NeuralNet net2;
        NeuralNet net1;
        int rate = 2;
        try {
            if (comboBox.getValue().equals("Compress")) {
                ObjectInputStream is1 = new ObjectInputStream(ClassLoader.getSystemResourceAsStream("TrainTogether"+3*rate*rate+"/Net1.ser"));
                net1 = (NeuralNet) is1.readObject();
                compress(net1, rate);
            } else {
                ObjectInputStream is2 = new ObjectInputStream(ClassLoader.getSystemResourceAsStream("TrainTogether"+3*rate*rate+"/Net2.ser"));
                net2 = (NeuralNet) is2.readObject();
                decompress(net2, rate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void stop() {
        if (thread != null) {
            thread.interrupt();
        }
    }

    private void compress(NeuralNet net1, int rate) {
        Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                ImageEdit edit = new ImageEdit();
                try {
                    for (File file : listIn.getItems()) {//TODO File Checker
                        if (!thread.isInterrupted()) {
                            Image image = new Image(new FileInputStream(file.getAbsolutePath()));
                            int imageWidth = (int) ((image.getWidth() / 4) * rate);
                            int imageHeight = (int) ((image.getHeight() / 4) * rate);
                            String format = getExtension(file);
                            double[][] patterns = edit.getRGB(edit.getSquare(file.getAbsolutePath(), 4));
                            int masLen = 3 * (int) Math.pow(rate, 2);
                            double[][] masOut = new double[patterns.length][masLen];
                            int x = 0;
                            int y = 0;
                            double [] buf;
                            for (double[] pat : patterns) {
                                net1.setInputLayer(pat);
                                buf = net1.directPass();
                                for (double d : buf) {
                                    masOut[x][y] = d;
                                    y++;
                                }
                                x++;
                                y = 0;
                            }
                            File compressedFile = edit.getImage(masOut, imageWidth, imageHeight, format, rate, savePath+"\\"+file.getName());
                            Platform.runLater(() -> {
                                if(!listOut.getItems().contains(compressedFile)){
                                    listOut.getItems().add(compressedFile);
                                }
                            });
                        } else {
                            throw new InterruptedException();
                        }
                    }
                } catch (InterruptedException e) {
                    System.out.println("Thread is interrupted!!!");
                }
                Platform.runLater(() -> {
                    progressIndicator.progressProperty().unbind();
                    progressIndicator.setProgress(1);
                });
                return null;
            }
        };
        progressIndicator.progressProperty().bind(task.progressProperty());
        thread = new Thread(task);
        thread.start();
    }

    private void decompress(NeuralNet net2, int rate) {
        Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                ImageEdit edit = new ImageEdit();
                try {
                    for (File file : listIn.getItems()) {//TODO File Checker
                        if (!thread.isInterrupted()) {
                            Image image = new Image(new FileInputStream(file));
                            int imageWidth = (int) ((image.getWidth() / rate) * 4);
                            int imageHeight = (int) ((image.getHeight() / rate) * 4);
                            String format = getExtension(file);
                            double[][] patterns = edit.getRGB(edit.getSquare(file.getAbsolutePath(), rate));
                            double[][] masOut = new double[patterns.length][48];
                            int x = 0, y = 0;
                            double [] buf;
                            for (double[] pat : patterns) {
                                net2.setInputLayer(pat);
                                buf = net2.directPass();
                                for (double d : buf) {
                                    masOut[x][y] = d;
                                    y++;
                                }
                                x++;
                                y = 0;
                            }
                            File decompressedFile = edit.getImage(masOut, imageWidth, imageHeight, format, 4, savePath+"\\"+file.getName());
                            Platform.runLater(() -> {
                                if(!listOut.getItems().contains(decompressedFile)){
                                    listOut.getItems().add(decompressedFile);
                                }
                            });
                        } else {
                            throw new InterruptedException();
                        }
                    }
                } catch (InterruptedException e) {
                    System.out.println("Thread is interrupted");
                }
                Platform.runLater(() -> {
                    progressIndicator.progressProperty().unbind();
                    progressIndicator.setProgress(1);
                });
                return null;
            }
        };
        progressIndicator.progressProperty().bind(task.progressProperty());
        thread = new Thread(task);
        thread.start();
    }

    private String getExtension(File file) {
        return file.getName().substring(file.getName().lastIndexOf(".") + 1);
    }
}
